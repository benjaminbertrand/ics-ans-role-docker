ics-ans-role-docker
===================

Ansible role to install docker on CentOS.

By default, docker-compose and the Python library for docker are installed.
This is required to use the docker modules in Ansible.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
docker_version: 18.09.0
docker_compose_version: 1.23.1
docker_users: []
docker_log_driver: json-file
docker_log_opts: {}
```

Ansible 2.2 currently requires the `docker-py` library (v1) and not the more recent `docker` one (v2).
Ansible 2.3.0.0 is compatible with docker-py v2 (renamed docker).
See https://github.com/ansible/ansible/commit/e2a1ce2916e9b87fe1ad4a1011c04ef8d2faf046

docker-compose 1.9.0 depends on docker-py 1.10.6.
If you set `docker_compose_version` to a version >= 1.10.0, it will install docker v2.
Set it to `none` if you don't want to install the Python library (only the docker daemon will be installed).

The users defined in the `docker_users` variable will be added to the docker group.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-docker
      docker_compose_version: none
```

License
-------

BSD 2-clause
