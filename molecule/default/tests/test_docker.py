# Generic docker tests
import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker_running_and_enabled(host):
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled


def test_docker_run(host):
    with host.sudo():
        cmd = host.command('docker run hello-world')
    assert cmd.rc == 0
    assert 'Hello from Docker!' in cmd.stdout


def test_docker_log_driver(host):
    with host.sudo():
        cmd = host.run("docker info")
        if 'syslog' in host.ansible.get_variables()['group_names']:
            assert "Logging Driver: syslog" in cmd.stdout
            assert re.search("[0-9a-fA-F]{12}\[[\d]+\]: Hello from Docker!", host.file('/var/log/messages').content)
        else:
            assert "Logging Driver: json-file" in cmd.stdout
            assert 'Hello from Docker!' not in host.file('/var/log/messages').content
