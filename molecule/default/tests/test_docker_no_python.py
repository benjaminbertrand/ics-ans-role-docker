# Tests for the no_python group (pip and docker Python library not installed)
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('no_python')


def test_pip_not_installed(host):
    assert not host.file('/usr/bin/pip').exists
