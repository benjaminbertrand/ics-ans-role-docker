# Tests for the docker Python library v1
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('python_v1')


def test_docker_python_library_v1_installed(host):
    packages = host.pip_package.get_packages(pip_path='/usr/bin/pip')
    assert 'docker-py' in packages
    assert 'docker' not in packages
    assert 'docker-compose' in packages
