# Tests for the docker Python library v2
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('python_v2')


def test_docker_python_library_v2_installed(host):
    packages = host.pip_package.get_packages(pip_path='/usr/bin/pip')
    assert 'docker-py' not in packages
    assert 'docker' in packages
    assert 'docker-compose' in packages
